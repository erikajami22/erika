<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('header');
		$this->load->view('welcome_message');
		$this->load->view('footer');
	}

	public function nosotro()
	{
		$this->load->view('header');
		$this->load->view('nosotros');
		$this->load->view('footer');
	}

	public function servicio()
	{
		$this->load->view('header');
		$this->load->view('servicio');
		$this->load->view('footer');
	}

	public function contacto()
	{
		$this->load->view('header');
		$this->load->view('contacto');
		$this->load->view('footer');
	}

	public function mision()
	{
		$this->load->view('header');
		$this->load->view('mision');
		$this->load->view('footer');
	}

	public function coagulacion()
	{
		$this->load->view('header');
		$this->load->view('coagulacion');
		$this->load->view('footer');
	}

	public function hematologia()
	{
		$this->load->view('header');
		$this->load->view('hematologia');
		$this->load->view('footer');
	}
}
