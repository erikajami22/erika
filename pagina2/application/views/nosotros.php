<br>
<div class="row">
<div class=""> <CENTER><h3>NOSOTROS</h3></center></div>
  <br><center>
  <img src="<?php echo base_url();?>/assets/ima/24.jpg" alt="" width="600px" height="200px">
  <div ><h1>Historia del Laboratorio Clínico </h1></center> <p>Los orígenes del laboratorio clínico
    El Laboratorio Clínico es el espacio físico donde se efectúan una gran diversidad de procedimientos médicos, científicos, técnicos, etc., que en conjunto representan un valioso recurso de la clínica al documentar el estado de salud (medicina preventiva) o de enfermedad (medicina curativa).

    El laboratorio clínico utiliza metodologías de diversas disciplinas como la bioquímica, hematología, inmunología y microbiología. En el laboratorio clínico se obtienen y se estudian muestras clínicas, como sangre, orina, heces, líquido sinovial (articulaciones), líquido cefalorraquídeo, exudados faríngeos y vaginales, entre otras.

    Estos son los principales servicios que ofrece un laboratorio clínico:
    <br>
    Descubrir enfermedades en etapas subclínicas
    Ratificar un diagnóstico sospechado clínicamente.
    Obtener información sobre el pronóstico de una enfermedad.
    Establecer un diagnóstico basado en una sospecha bien definida.
    Vigilar un tratamiento o conocer una determinada respuesta terapéutica.
    Precisar factores de riesgo.
    Ahora que ya sabemos qué es un laboratorio clínico vamos a indagar un poco en su historia. Nos tenemos que remontar hasta 1790 para hablar del primer laboratorio clínico de la historia. Se trataba de un espacio muy simple y rudimentario con un médico que observaba e interpretaba el fluido, en este caso la orina de un paciente en un frasco de vidrio.
    <br>
    El valor diagnóstico de la orina fue convirtiéndose en una prueba sistemática avalada por la experiencia médica. El médico Jacob Isaac Ben Salomón Al Israelí (c860-953) escribió el libro traducido en la Escuela Salernitana del siglo XI por Constantino el Africano (c1010-1087) bajo el título de Liber de urinis. Un siglo después, el Rector y Gran Maestre de la Escuela, Mauro de Salerno (c1130-1214), sistematizaría la uroscopia con su obra Regula urinarius.
    <br>
    Estas dos publicaciones suponen el origen de la ciencia del laboratorio clínico que supondrá un respaldo a la medicina en cuanto a los diagnósticos.</p> </div>
</div>
