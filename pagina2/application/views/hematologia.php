<div class="row"><center><h1>EMATOLOGIA</h1></CENTER></div>
<div class="row">
  <div class="col-md-6"> <img src="<?php echo base_url();?>/assets/ima/20.jpg" alt="servicio" width="600px" height="300px"> </div>
  <div class="col-md-5"> <p> La hematología es una especialidad de la medicina que estudia los elementos inmunológicos de la sangre y las
     enfermedades que se manifiestan por la alteración de estos elementos. También estudia los órganos hematopoyéticos para la etiología,
      diagnóstico, tratamiento, pronóstico y prevención de posibles enfermedades.</p></div>
</div>
<div class="row">
  <div class="col-md-6"> <img src="<?php echo base_url();?>/assets/ima/22.jpg" alt="" width="600px" height="300px"><br>
    <br> <p> También se realizan análisis clínicos de otros parámetros como el screening
     de pruebas de coagulación y pruebas especiales, que ayudan a determinar trastornos  <br>en la coagulación sanguínea y su seguimiento.

    Los datos obtenidos pueden llegar a resultar de gran importancia en determinadas situaciones patológicas,
    de mayor o menor entidad clínica, siempre acompañados de estudios citológicos e inmunohematológicos.</p>
  </div>
  <div class="">

  </div>
  <div class="col-md-6"> <img src="<?php echo base_url();?>/assets/ima/23.jpg" alt="" width="600px" height="300px">
    <p>La Unidad de Hematología de Laboratorio Echevarne ofrece un servicio <br>
       hematológico de hemostasia, coagulación y pruebas especiales. Se dedica fundamentalmente al
       estudio elemental de la sangre, determinación en que se mide de forma global y en porcentaje
       la serie eritrocitaria (roja), leucocitaria (blanca) y plaquetar.</p>  </div>
</div>
